﻿using OpenQA.Selenium;

namespace Frames
{
    class JSBinPage
    {
        private IWebDriver _driver;
        public JSBinPage(IWebDriver driver)
        {
            _driver = driver;
        }
        public IWebElement JSBinOutParentFrame => _driver.FindElement(By.XPath("//iframe[@class='stretch']"));
        public IWebElement JSBinOutFrame => _driver.FindElement(By.XPath("//iframe[@name='JS Bin Output ']"));
        public IWebElement AddedInputField => _driver.FindElement(By.XPath("//*[@id='test']"));
    }
}
