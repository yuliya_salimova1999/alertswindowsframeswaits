using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;

namespace Frames
{
    public class Tests
    {
        private IWebDriver _driver;
        public IWebDriver Driver => _driver;
        [SetUp]
        public void Setup()
        {
            _driver = new ChromeDriver();
        }

        [Test]
        public void Test1()
        {
            const string jsBinUrl = "https://jsbin.com/?html,output";
            Driver.Navigate().GoToUrl(jsBinUrl);
            JSBinPage jsBinPage = new JSBinPage(Driver);
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            new Actions(Driver).SendKeys("<input id=\"test\"/>").Build().Perform();
            new Actions(Driver).MoveToElement(jsBinPage.JSBinOutParentFrame).Build().Perform();
            Driver.SwitchTo().Frame(jsBinPage.JSBinOutParentFrame);
            IWebElement jsFrame = wait.Until(frame => frame.FindElement(By.XPath("//iframe[@name='JS Bin Output ']")));
            Driver.SwitchTo().Frame(jsFrame);
            IWebElement inputField = wait.Until(input => input.FindElement(By.XPath("//input[@id='test']")));
            jsBinPage.AddedInputField.SendKeys("test");
            Assert.AreEqual("test", inputField.Text);
        }

        [TearDown]
        public void Dispose()
        {
            Driver.Quit();
        }
    }
}