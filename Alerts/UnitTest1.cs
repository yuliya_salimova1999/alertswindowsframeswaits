using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace Alerts
{
    [TestFixture]
    public class Tests
    {
        private IWebDriver _driver;
        public IWebDriver Driver => _driver;
        [SetUp]
        public void Setup()
        {
            Console.Out.WriteLine("Starting the browser...");
            _driver = new ChromeDriver();
        }

        [Test]
        public void Test1()
        {
            const string alertsUrl = "https://www.toolsqa.com/handling-alerts-using-selenium-webdriver/";
            Driver.Navigate().GoToUrl(alertsUrl);
            ToolsQAPage toolsQAPage = new ToolsQAPage(Driver);
            IJavaScriptExecutor executor = (IJavaScriptExecutor)Driver;
            executor.ExecuteScript("arguments[0].click()", toolsQAPage.SimpleAlertButton);
            var simpleAlert = Driver.SwitchTo().Alert();
            Console.Out.WriteLine(simpleAlert.Text);
            simpleAlert.Accept();
            executor.ExecuteScript("arguments[0].click()", toolsQAPage.ConfirmPopUpButton);
            var confirmAlert = Driver.SwitchTo().Alert();
            Console.Out.WriteLine(confirmAlert.Text);
            confirmAlert.Accept();
            executor.ExecuteScript("arguments[0].click()", toolsQAPage.ConfirmPopUpButton);
            confirmAlert = Driver.SwitchTo().Alert();
            Console.Out.WriteLine(confirmAlert.Text);
            confirmAlert.Dismiss();
            executor.ExecuteScript("arguments[0].click()", toolsQAPage.PromptPopUpButton);
            var promptAlert = Driver.SwitchTo().Alert();
            Console.Out.WriteLine(promptAlert.Text);
            promptAlert.SendKeys("Great site");
            promptAlert.Accept();
        }
        [TearDown]
        public void Dispose()
        {
            Driver.Quit();
        }
    }
}