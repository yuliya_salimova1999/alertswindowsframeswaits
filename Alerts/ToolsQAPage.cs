﻿using OpenQA.Selenium;

namespace Alerts
{
    class ToolsQAPage
    {
        private IWebDriver _driver;
        public ToolsQAPage(IWebDriver driver)
        {
            _driver = driver;
        }
        public IWebElement SimpleAlertButton => _driver.FindElement(By.XPath("//button[text()='Simple Alert']"));
        public IWebElement ConfirmPopUpButton => _driver.FindElement(By.XPath("//button[text()='Confirm Pop up']"));
        public IWebElement PromptPopUpButton => _driver.FindElement(By.XPath("//button[text()='Prompt Pop up']"));
    }
}
