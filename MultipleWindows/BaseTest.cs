﻿using NUnit.Allure.Core;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace MultipleWindows
{
    [TestFixture]
    [AllureNUnit]
    public class BaseTest : IDisposable
    {
        protected IWebDriver Driver { get; set; }

        [SetUp]
        public void SetUp()
        {
            Driver = new ChromeDriver();
        }

        [TearDown]
        public void Dispose()
        {
            Driver.Quit();
        }
    }
}
