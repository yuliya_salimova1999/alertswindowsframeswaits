using MultipleWindows.Pages;
using NUnit.Framework;
using System;

namespace MultipleWindows
{
    public class Tests : BaseTest
    {
        private MainPage _mainPage;
        private AppleStorePage _appleStorePage;
        private GooglePlayPage _googlePlayPage;
        [Test]
        public void Test1()
        {
            const string MainUrl = "https://catalog.onliner.by/tv";
            Driver.Navigate().GoToUrl(MainUrl);
            _mainPage = new MainPage(Driver);
            _mainPage.ClickAppleStoreButton();
            _appleStorePage = new AppleStorePage(Driver);
            Driver.SwitchTo().Window(_mainPage.WindowHandle);
            _mainPage.ClickGooglePlayButton();
            _googlePlayPage = new GooglePlayPage(Driver);
            Assert.AreEqual(3, Driver.WindowHandles.Count);
            Driver.SwitchTo().Window(_googlePlayPage.WindowHandle);
            Assert.IsTrue(_googlePlayPage.CheckAppName("������� Onliner"));
            _googlePlayPage.ClickSimilarAppButton();
            Assert.IsTrue(_googlePlayPage.IsSimilarAppButtonClicked());
            Console.Out.WriteLine(_googlePlayPage.GetSimilarAppsCount());
            Driver.SwitchTo().Window(_appleStorePage.WindowHandle);
            _appleStorePage.ClickMoreButton();
            Assert.IsTrue(_appleStorePage.IsMoreButtonClicked());
            Driver.SwitchTo().Window(_mainPage.WindowHandle);
            _mainPage.ClickAdvertismentItem();
        }
    }
}