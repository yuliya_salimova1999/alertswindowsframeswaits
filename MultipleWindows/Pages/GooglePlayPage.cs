﻿using OpenQA.Selenium;
using System.Collections.ObjectModel;

namespace MultipleWindows.Pages
{
    class GooglePlayPage : BasePage
    {
        private IWebElement SimilarAppButton => _driver.FindElement(By.XPath("//a[@data-uitype='290']"));
        private IWebElement AppName => _driver.FindElement(By.XPath("//*[@itemprop='name']/span"));
        private ReadOnlyCollection<IWebElement> SimilarAppsList => _driver.FindElements(By.XPath("//div[@class='ImZGtf mpg5gc']"));

        public GooglePlayPage(IWebDriver driver) : base(driver)
        {
        }

        public bool CheckAppName(string appName) => AppName.Text == appName;

        public void ClickSimilarAppButton() => SimilarAppButton.Click();

        public bool IsSimilarAppButtonClicked() => SimilarAppsList != null;

        public int GetSimilarAppsCount() => SimilarAppsList.Count;
    }
}
