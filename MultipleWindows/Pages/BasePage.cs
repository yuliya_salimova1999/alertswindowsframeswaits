﻿using OpenQA.Selenium;
using System.Linq;

namespace MultipleWindows.Pages
{
    class BasePage
    {
        protected IWebDriver _driver;
        public string WindowHandle { get; }

        public BasePage(IWebDriver driver)
        {
            _driver = driver;
            WindowHandle = driver.WindowHandles.Last();
        }
    }
}
