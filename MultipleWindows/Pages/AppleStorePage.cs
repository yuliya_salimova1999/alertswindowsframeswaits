﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace MultipleWindows.Pages
{
    class AppleStorePage : BasePage
    {
        private IWebElement MoreButton => new WebDriverWait(_driver, TimeSpan.FromSeconds(10))
            .Until(button=>button.FindElement(By.XPath("//*[@id='ember209']/button")));

        public AppleStorePage(IWebDriver driver) : base(driver)
        {
        }

        public void ClickMoreButton() => MoreButton.Click();

        public bool IsMoreButtonClicked()
        {
            if (_driver.FindElement(By.Id("ember209")).GetAttribute("class").Contains("we-truncate--full")) return true;
            return false;
        }
    }
}
