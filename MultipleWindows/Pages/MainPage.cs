﻿using OpenQA.Selenium;

namespace MultipleWindows.Pages
{
    class MainPage : BasePage
    {
        private IJavaScriptExecutor _executor;
        private IWebElement AppleStoreLink => _driver.FindElement(By.XPath("//a[contains(@class,'store-item_apple')]"));
        private IWebElement GooglePlayLink => _driver.FindElement(By.XPath("//a[contains(@class,'store-item_google')]"));
        private IWebElement AdvertisementItem => _driver.FindElement(By.XPath("//*[@class='schema-product__image'][1]/a"));
        public MainPage(IWebDriver driver) : base(driver)
        {
            _executor = (IJavaScriptExecutor)_driver;
        }

        public void ClickAppleStoreButton() => JSClick(AppleStoreLink);

        public void ClickGooglePlayButton() => JSClick(GooglePlayLink);

        public void ClickAdvertismentItem() => JSClick(AdvertisementItem);

        private void JSClick(IWebElement element) => _executor.ExecuteScript("arguments[0].click()", element);
    }
}
