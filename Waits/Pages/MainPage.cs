﻿using OpenQA.Selenium;

namespace Waits.Pages
{
    class MainPage : BasePage
    {
        public MainPage(IWebDriver driver) : base(driver)
        {
        }
        public IWebElement FirstComparisonItem => _driver.FindElement(By.XPath("//div[@id='schema-products']/child::div[1]/*/*/div[@class='schema-product__compare']//input"));
        public IWebElement SecondComparisonItem => _driver.FindElement(By.XPath("//div[@id='schema-products']/child::div[2]/*/*/div[@class='schema-product__compare']//input"));
        public IWebElement CompareButton => _driver.FindElement(By.XPath("//div[@class='compare-button-container']//a[@class=\"compare-button__sub compare-button__sub_main\"]"));
    }
}
