﻿using OpenQA.Selenium;

namespace Waits.Pages
{
    class CompareProductsPage : BasePage
    {
        public const string ProductTableTipTextLocator = "//div[@id='productTableTip']";
        public const string ProductTableTipIconlocator = "//span[@data-tip-term='Диагональ экрана']";
        public CompareProductsPage(IWebDriver driver) : base(driver)
        {
        }
        public IWebElement TableComparisonParametr => _driver.FindElement(By.XPath("//td[@class='product-table__cell']/span[text()='Диагональ экрана']"));
        public IWebElement DeleteComparisonItemIcon => _driver.FindElement(By.XPath("//div[@class='product-summary']/following-sibling::a[@title='Удалить'][1]"));
    }
}
