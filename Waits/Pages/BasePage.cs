﻿using OpenQA.Selenium;

namespace Waits.Pages
{
    class BasePage
    {
        protected IWebDriver _driver;
        public BasePage(IWebDriver driver)
        {
            _driver = driver;
        }
    }
}
