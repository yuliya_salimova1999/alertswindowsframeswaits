using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using Waits.Pages;

namespace Waits
{
    public class Tests
    {
        private IWebDriver _driver;
        public IWebDriver Driver => _driver;

        [SetUp]
        public void Setup()
        {
            const string MainUrl = "https://catalog.onliner.by/tv";
            _driver = new ChromeDriver();
            Driver.Navigate().GoToUrl(MainUrl);
        }

        [Test]
        public void Test1()
        {
            MainPage mainPage = new MainPage(Driver);
            new Actions(Driver).Click(mainPage.FirstComparisonItem).Build().Perform();
            new Actions(Driver).Click(mainPage.SecondComparisonItem).Build().Perform();
            Driver.Navigate().GoToUrl(mainPage.CompareButton.GetAttribute("href"));
            CompareProductsPage comparePage = new CompareProductsPage(Driver);
            new Actions(Driver).MoveToElement(comparePage.TableComparisonParametr).Build().Perform();
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement productTableTipIcon = wait.Until(icon => icon.FindElement(By.XPath(CompareProductsPage.ProductTableTipIconlocator)));
            new Actions(Driver).Click(productTableTipIcon).Build().Perform();
            IWebElement productTableTipText = wait.Until(text => text.FindElement(By.XPath(CompareProductsPage.ProductTableTipTextLocator)));
            new Actions(Driver).Click(productTableTipIcon).Build().Perform();
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath(CompareProductsPage.ProductTableTipTextLocator)));
            new Actions(Driver).Click(comparePage.DeleteComparisonItemIcon).Build().Perform();
            ((IJavaScriptExecutor)Driver).ExecuteScript("return window.jQuery != undefined && jQuery.active == 0");
        }

        [TearDown]
        public void Dispose()
        {
            Driver.Quit();
        }
    }
}